module Lib
    ( someFunc,
    isOdd,
    myMap
    ) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

isOdd :: Int -> Bool
isOdd x = mod x 2 == 1

myMap :: (a->b) -> [a] -> [b] -- takes a func , and convert list (with elems being type a) to list (with elems being type b)
myMap f (x:xs) = f x : myMap f xs 

-- if ommited this, will get this error:
-- mymap-exe: src/Lib.hs:15:1-33: Non-exhaustive patterns in function myMap
myMap f [] = [] 
