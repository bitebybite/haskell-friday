module Lib
    ( 
      square,
      base10pow,
    ) where


-- The ** operator takes 2 argument:
-- 5**2 is 25
-- we can give 1 argument to ** to get a "curry function"
square ::  Float -> Float
square = (**2)
    
base10pow :: Float -> Float 
base10pow = (10**)
